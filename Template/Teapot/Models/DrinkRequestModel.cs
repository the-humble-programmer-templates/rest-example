namespace Teapot.Models
{
    public class DrinkRequestModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}